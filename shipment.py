# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.pool import PoolMeta
from trytond.transaction import Transaction
from trytond.wizard import (
    Wizard, StateTransition, StateView, StateReport, Button, StateAction)


class DeliveryNote(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.delivery_note'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        report_context['valued'] = data.get('valued', True)
        return report_context


class PrintValuedDeliveryNoteStart(ModelView):
    """Start Print delivery note valued"""
    __name__ = 'stock.shipment.out.delivery_note.print_valued.start'

    valued = fields.Boolean('Valued')

    @classmethod
    def default_valued(cls):
        return True


class PrintValuedDeliveryNote(Wizard):
    """Print delivery note valued"""
    __name__ = 'stock.shipment.out.delivery_note.print_valued'

    start = StateView('stock.shipment.out.delivery_note.print_valued.start',
        'stock_valued_report.delivery_note_print_valued_start_view_form',
        [Button('Not valued', 'set_non_valued', 'tryton-list-remove'),
         Button('Valued', 'set_valued', 'tryton-list-add', default=True)])
    set_valued = StateTransition()
    set_non_valued = StateTransition()
    print_ = StateReport('stock.shipment.out.delivery_note')

    def transition_set_valued(self):
        self.start.valued = True
        return 'print_'

    def transition_set_non_valued(self):
        self.start.valued = False
        return 'print_'

    def do_print_(self, action):
        data = {
            'valued': self.start.valued,
            'id': Transaction().context['active_ids'].pop()
        }
        data['ids'] = [data['id']]
        return action, data

    def transition_print_(self):
        return 'end'


class PrintValuedRestockingList(PrintValuedDeliveryNote):
    """Print delivery note valued"""
    __name__ = 'stock.shipment.out.return.restocking_list.print_valued'

    print_ = StateReport('stock.shipment.out.return.restocking_list')


class ShipmentOutDoAndPrint(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.go_and_print'

    @classmethod
    def __setup__(cls):
        super(ShipmentOutDoAndPrint, cls).__setup__()
        cls.print_ = StateAction(
            'stock_valued_report.wizard_delivery_note_valued')
